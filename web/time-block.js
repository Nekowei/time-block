var currentDay = 0
var currentDate
var ctx = ""
var offset = -4 // 提前4小时偏移

$(function () {

    $.ajax("/user")
        .done(function (data) {
            $("#username").text(data)
        })
        .fail(function (xhr) {
            if (xhr.status == 401) {
                location = "/login"
            }
        })

    loadType()

    $("#type-add-color").change(e => {
        $("#type-add-button").attr("style", "background:" + $(e.target).val())
    })
    $("#type-add-select").change(e => {
        let color = $("#type-add-select option:selected").attr("color")
        $("#type-add-button").attr("style", "background:" + color)
        $("#type-add-color").val(color)
    })

    $("#type-add-div").hide()

    $("#type-add-submit").submit(e => {
        $("#type-add-button").attr("disabled", "disabled")
        e.preventDefault()
        let t = $(e.target)
        let formData = t.serializeArray()
        $.post(t.attr("action"), formData, () => { }, "json")
            .always(function (dx, status, xe) {
                $("#type-add-button").removeAttr("disabled")
                loadType()
            })
    })

    changeDay(currentDay)

})

function loadType() {
    $.get(ctx + "/type/tree", data => {
        $("#type-div").html("")
        $("#type-add-select").html("<option value='' color='#fff'>-</option>")
        data.forEach(updateTypeUI)

        $(".type-button").click(e => {
            let t = $(e.target)
            $("#current-type").val(t.val())
            $("#current-type").attr("style", t.attr("style"))
            $("#current-type").html(t.html())
        })
        $(".type-button-edit").click(e => {
            let t = $(e.target)
            console.log(t.val())
            if ($("#type-add-id").val()) {
                $("#type-add-id").val(t.val())
            } else {
                $("#type-add-submit").append("<input id='type-add-id' type='hidden' name='id' value='" + t.val() + "'/>")
            }
            $("#type-add-name").val(t.attr('iname'))
            $("#type-add-color").val(t.attr('icolor'))
        })
        $(".type-button-delete").click(e => {
            $(".type-button-delete").attr("disabled", "disabled")
            $.post(ctx + "/type/delete", { id: $(e.target).val() }, () => { }, "json")
                .done(function (data, status, xhr) {
                })
                .fail(function (xhr, status, e) {
                })
                .always(function (dx, status, xe) {
                    loadType()
                })
        })
        $(".type-button-edit").hide()
        $(".type-button-delete").hide()
    })
}

// for each type
function updateTypeUI(t) {
    $("#type-div").append("<div id='type-" + t.id + "' class='type-div'></div>")
    let tid = "#type-" + t.id
    $(tid).append("<div class='type major'>"
        + "<button value='" + t.id + "' style='background:" + t.color + "' class='type-button type-block'>" + t.name + "</button>"
        + "<button value='" + t.id + "' iname='" + t.name + "' icolor='" + t.color + "' style='background:" + t.color + "' class='type-button-edit'>&#128296;</button>"
        + "<button value='" + t.id + "' style='background:" + t.color + "' class='type-button-delete'>&#10060;</button>"
        + "</div>")
    t.detail.forEach(d => {
        $(tid).append("<div class='type minor'>"
            + "<button value='" + d.id + "' style='background:" + d.color + "' class='type-button type-block'>" + d.name + "</button>"
            + "<button value='" + d.id + "' iname='" + d.name + "' icolor='" + d.color + "' style='background:" + d.color + "' class='type-button-edit'>&#128296;</button>"
            + "<button value='" + d.id + "' style='background:" + d.color + "' class='type-button-delete'>&#10060;</button>"
            + "</div>")
    })

    // fill the select list by the way
    $("#type-add-select").append("<option value='" + t.id + "' color='" + t.color + "'>" + t.name + "</option>")
}

function changeDay(day) {
    currentDay += day
    $(".change-button").attr("disabled", "disabled")
    $.get(ctx + "/block/date", { day: currentDay }, d => {
        currentDate = d.substring(0, 10)
        $(".current-date").each(function (i, t) { $(t).text(d) })
    })
    $.get(ctx + "/block/list", { day: currentDay }, getBlockList)
    $.get(ctx + "/stat/month", { day: currentDay }, function (data) { updateStatDiv(data, "#month-div", 2) })
    $.get(ctx + "/stat/monthMajor", { day: currentDay }, function (data) { updateStatDiv(data, "#monthMajor-div", 1) })
    $.get(ctx + "/stat/week", { day: currentDay }, function (data) { updateStatDivList(data, "#week-div", 4) })
    $.get(ctx + "/stat/recent30", { day: currentDay }, getRecent30)
}

function changeDayAt(dir, id) {
    changeDay(dir * $("#" + id).val())
}

function recent() {
    var length = $("#recent-length").val()
    $.get(ctx + "/stat/recent30", { day: currentDay, length: length }, getRecent30)
}

function getBlockList(data) {
    $(".change-button").removeAttr("disabled")
    $("#time-list").html("")
    for (const [hour, list] of Object.entries(data)) {
        let div = "<div class='time-list-hour'>"
            + "<button class='time-list-hour-button'>" + hour + "</button>"
        for (let i = 0; i < list.length; i++) {
            const m = list[i];
            div += "<button class='time-list-minute-button type-block' style='background:" + m.color
                + "' start='" + m.startTime + "' end='" + m.endTime + "'>" + m.name + "</button>"
        }
        div += "</div>"
        $("#time-list").append(div)
    }
    $(".time-list-hour-button").click(onHourButtonClick)
    $(".time-list-minute-button").click(onMinuteButtonClick)
}

function updateStatDiv(data, selector, factor) {
    $(selector).html("")
    let sum = 0
    for (let i = 0; i < data.length; i++) {
        sum += data[i].count
    }
    for (let i = 0; i < data.length; i++) {
        let vo = data[i]
        let avg = (Math.round(vo.count * 100 / sum)) + '%'
        let div = "<div class='stat-bar-container'>"
            + "<div class='stat-bar-span-div stat-bar-col1'><span class='stat-bar-span'>" + vo.name + "</span></div>"
            + "<div class='stat-bar-span-div stat-bar-col2'><span class='stat-bar-span'>" + (vo.count / 4) + "</span></div>"
            + "<div class='stat-bar-span-div stat-bar-col3'><span class='stat-bar-span'>" + avg + "</span></div>"
            + "<div class='stat-bar' style='background:" + vo.color + "; width:" + (vo.count * factor) + "px;'>"
            + "</div></div>"
        $(selector).append(div)
    }
}

function updateStatDivList(data, selector, factor) {
    $(selector).html("")
    for (let i = 0; i < data.length; i++) {
        let typeMap = data[i]
        let div = "<div class='stat-bar-div'>"
        div += "<span>" + typeMap.type + "</span>"
        for (let j = 0; j < typeMap.list.length; j++) {
            let vo = typeMap.list[j]
            div += "<div class='stat-bar-container'><span class='stat-bar-span'>" + (vo.count / 4) + "</span>"
                + "<div class='stat-bar' style='background:" + vo.color + "; width:" + (vo.count * factor) + "px; top:-20px;'>"
                + "</div></div>"
        }
        $(selector).append(div + "</div>")
    }
}

function getRecent30(data) {
    $("#30-div").html("")

    let size = 9
    let w = 96 * size
    let h = (Math.ceil(data.length / 96) + 3) * size
    let html = "<svg class='stat-30-container' width='" + w + "' height='" + h + "px'>"

    for (let i = 0; i < 24; i++) {
        let xpos = i * size * 4
        let t = i - offset
        if (t > 24) {
            t = t - 24
        }
        if (t < 0) {
            html += "<text class='stat-30' x='" + xpos + "' y='15'>" + t + "_</text>"
        }
        else if (t < 10) {
            html += "<text class='stat-30' x='" + xpos + "' y='15'>_0" + t + "_</text>"
        } else {
            html += "<text class='stat-30' x='" + xpos + "' y='15'>_" + t + "_</text>"
        }
    }

    if (offset < 24 && offset > -24) {
        if (offset < 0) {
            for (let i = 0; i < (24 + offset) * 4; i++) {
                data.splice(i, 0, { name: null, color: "#fff", count: null })
            }
        } else {
            for (let i = 0; i < offset * 4; i++) {
                data.splice(i, 0, { name: null, color: "#fff", count: null })
            }
        }
    }

    for (let i = 0; i < data.length; i++) {
        let vo = data[i]
        let row = i % 96
        let x = (row) * size
        let rua = Math.ceil(i / 96)
        let y = (i % 96 > 0 ? (rua + 1) : (rua + 2)) * size
        html += "<rect class='stat-30' x='" + x + "' y='" + y + "' width='" + size + "' height='" + size + "'"
            + "' rowNo='" + row + "' style='fill:" + vo.color + ";'>_</rect>"
    }

    $("#30-div").append(html + "</svg>")

}

function onHourButtonClick(e) {
    if ($("#current-type").val()) {
        let t = $(e.target)
        let text = $("#current-type").text()
        let style = $("#current-type").attr("style")
        $.post(ctx + "/block/save/all", {
            typeId: $("#current-type").val(),
            recordDate: currentDate,
            hour: t.text()
        }, () => { }, "json")
            .done(function (d, status, x) {
                console.log(text + ":" + status)
                let n1 = t.next()
                setText(n1, text, style)

                let n2 = t.next().next()
                setText(n2, text, style)

                let n3 = t.next().next().next()
                setText(n3, text, style)

                let n4 = t.next().next().next().next()
                setText(n4, text, style)
            })
            .fail(function (x, status, e) {
                setText(t, status, style)
            })
    }
}

function onMinuteButtonClick(e) {
    if ($("#current-type").val()) {
        let t = $(e.target)
        let text = $("#current-type").text()
        let style = $("#current-type").attr("style")
        $.post(ctx + "/block/save", {
            typeId: $("#current-type").val(),
            recordDate: currentDate,
            startTime: t.attr("start"),
            endTime: t.attr("end")
        }, () => { }, "json")
            .done(function (d, status, x) {
                console.log(text + ":" + status)
                setText(t, text, style)
            })
            .fail(function (x, status, e) {
                setText(t, status, style)
            })

    }
}

function setText(t, text, style) {
    t.text(text)
    t.attr("style", style)
}

function major() {
    $.get(
        ctx + "/stat/major",
        { from: $("#major-from").val(), to: $("#major-to").val() },
        function (data) { updateStatDiv(data, "#monthMajor-div", 1) }
    )
}
