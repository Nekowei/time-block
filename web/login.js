$(function() {

    $(".login-submit").on("click", function(e) {
        e.preventDefault();
        var form = $(this);
        var data = {
           username: $("#username").val(),
           password: $("#password").val()
       }
        $.ajax({
            contentType: "application/json",
            type: "POST",
            url: "/login/process?username=" + data.username + "&password=" + data.password,
            data: JSON.stringify(data),
            dataType: "json"
        })
        .always(function(d, s, e) {
            console.log(d);
            console.log("always "+s);
            console.log("always "+e);
            window.location.href = "/";
        })
    });

})