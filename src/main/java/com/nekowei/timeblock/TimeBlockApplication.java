package com.nekowei.timeblock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.nekowei.timeblock")
public class TimeBlockApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimeBlockApplication.class, args);
	}

}
