package com.nekowei.timeblock.service;

import com.nekowei.timeblock.entity.BlockEntity;
import com.nekowei.timeblock.entity.BlockTypeEntity;
import com.nekowei.timeblock.repo.BlockRepository;
import com.nekowei.timeblock.util.UserUtil;
import com.nekowei.timeblock.vo.BlockVo;
import com.nekowei.timeblock.vo.StatVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class BlockService {

    @Autowired
    private BlockRepository blockRepository;
    @Autowired
    private BlockTypeService blockTypeService;

    @Transactional
    public BlockEntity save(BlockEntity s) {
        int typeId = s.getTypeId();
        s.setTypeId(null);
        blockRepository.findOne(Example.of(s))
                .ifPresent(old -> s.setId(old.getId()));
        s.setTypeId(typeId);
        return blockRepository.save(s);
    }

    @Transactional(readOnly = true)
    public Map<Integer, List<BlockVo>> list(LocalDate date, String username) {
        List<BlockEntity> list = blockRepository.findAll(Example.of(BlockEntity.builder()
                .recordDate(date.toString())
                .username(username)
                .build()));
        List<BlockTypeEntity> types = blockTypeService.list(username);
        for (int i = 0; i < 24; i++) {
            for (int j = 0; j < 4; j++) {
                BlockEntity e = new BlockEntity();
                e.setRecordDate(date.toString());
                e.setStartTime(LocalTime.of(i, j * 15).format(DateTimeFormatter.ISO_LOCAL_TIME));
                e.setEndTime(LocalTime.of(i, j * 15).plusMinutes(15).format(DateTimeFormatter.ISO_LOCAL_TIME));
                if (list.stream()
                    .noneMatch(be -> be.getStartTime().equals(e.getStartTime()))) {
                    list.add(e);
                }
            }
        }
        return list.stream()
                .map(this::copy)
                .peek(vo -> {
                    if (vo.getTypeId() != null) {
                        types.stream().filter(e -> e.getId().equals(vo.getTypeId()))
                        .findAny().ifPresent(t -> {
                            vo.setName(t.getName());
                            vo.setColor(t.getColor());
                        });
                    }
                })
                .sorted(Comparator.comparing(v -> v.getStartTime().toString()))
                .collect(Collectors.groupingBy(e -> e.getStartTime().getHour()));
    }

    private BlockVo copy(BlockEntity e) {
        BlockVo vo = new BlockVo();
        BeanUtils.copyProperties(e, vo);
        vo.setRecordDate(LocalDate.parse(e.getRecordDate()));
        vo.setStartTime(LocalTime.parse(e.getStartTime()));
        vo.setEndTime(LocalTime.parse(e.getEndTime()));
        return vo;
    }

    @Transactional
    public List<BlockEntity> saveAll(BlockVo e, String username) {
        return IntStream.range(0, 4).mapToObj(i -> {
            BlockEntity be = new BlockEntity();
            be.setUsername(username);
            be.setRecordDate(e.getRecordDate().toString());
            be.setStartTime(LocalTime.of(e.getHour(), i * 15).format(DateTimeFormatter.ISO_LOCAL_TIME));
            be.setEndTime(LocalTime.of(e.getHour(), i * 15).plusMinutes(15).format(DateTimeFormatter.ISO_LOCAL_TIME));
            blockRepository.findOne(Example.of(be))
                    .ifPresent(old -> be.setId(old.getId()));
            be.setTypeId(e.getTypeId());
            return blockRepository.save(be);
        }).collect(Collectors.toList());
    }

    public void delete(BlockEntity e) {
        blockRepository.delete(e);
    }

    public List<StatVo> stat(LocalDate from, LocalDate to) {
        List<BlockTypeEntity> types = blockTypeService.list();
        return statBy(from.toString(), to.toString()).stream()
                .collect(Collectors.groupingBy(BlockEntity::getTypeId))
                .entrySet().stream()
                .map(e -> {
                    StatVo vo = new StatVo();
                    types.stream()
                        .filter(t -> t.getId().equals(e.getKey()))
                        .findAny().ifPresent(t -> {
                            vo.setColor(t.getColor());
                            vo.setName(t.getName());
                    });
                    vo.setCount(e.getValue().size());
                    return vo;
                })
                .sorted(Comparator.comparing(StatVo::getCount).reversed())
                .collect(Collectors.toList());
    }

    public List<StatVo> statDot(LocalDate from, LocalDate to) {
        List<BlockTypeEntity> types = blockTypeService.list();
        return statBy(from.toString(), to.toString()).stream()
                .map(e -> {
                    StatVo vo = new StatVo();
                    types.stream()
                        .filter(t -> t.getId().equals(e.getTypeId()))
                        .findAny().ifPresent(t -> vo.setColor(t.getColor()));
                    return vo;
                })
                .collect(Collectors.toList());
    }

    public List<StatVo> statMajor(LocalDate from, LocalDate to) {
        List<BlockTypeEntity> types = blockTypeService.list();
        return statBy(from.toString(), to.toString()).stream()
                .collect(Collectors.groupingBy(e -> {
                    // all entity treat as major type
                    BlockTypeEntity type = types.stream()
                            .filter(t -> t.getId().equals(e.getTypeId()))
                            .findAny().orElseThrow(() -> new NullPointerException("type not found"));
                    return type.getParentId() != null ? type.getParentId() : type.getId();
                }))
                .entrySet().stream()
                .map(e -> {
                    StatVo vo = new StatVo();
                    types.stream()
                            .filter(t -> t.getId().equals(e.getKey()))
                            .findAny().ifPresent(t -> {
                        vo.setColor(t.getColor());
                        vo.setName(t.getName());
                    });
                    vo.setCount(e.getValue().size());
                    return vo;
                })
                .sorted(Comparator.comparing(StatVo::getCount).reversed())
                .collect(Collectors.toList());
    }

    private List<BlockEntity> statBy(String from, String to) {
        return blockRepository.findByUsernameAndRecordDateBetween(
                UserUtil.getUsername(), from.toString(), to.toString(),
                Sort.by("recordDate", "startTime"));
    }

}
