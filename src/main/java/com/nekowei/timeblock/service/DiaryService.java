package com.nekowei.timeblock.service;

import com.nekowei.timeblock.entity.DiaryEntity;
import com.nekowei.timeblock.repo.DiaryRepository;
import com.nekowei.timeblock.util.DateTimeUtil;
import com.nekowei.timeblock.vo.DiaryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DiaryService {

    @Autowired
    private DiaryRepository diaryRepository;

    public DiaryEntity save(DiaryEntity e) {
        e.setRecordDateTime(DateTimeUtil.now());
        diaryRepository.save(e);
        return e;
    }

    public List<DiaryVo> list(String username) {
        var list = diaryRepository.findAll(Example.of(DiaryEntity.builder()
                .username(username)
                .build()));
        return list.stream()
                .collect(Collectors.groupingBy(d -> DateTimeUtil.toMonth(DateTimeUtil.parse(d.getRecordDateTime()))))
                .entrySet().stream()
                .map(e -> new DiaryVo(e.getKey(),
                        e.getValue().stream()
                        .sorted(Comparator.comparing(DiaryEntity::getRecordDateTime))
                        .map(DiaryEntity::getContent)
                        .toList())
                )
                .toList();
    }

    public void delete(Integer id) {
        diaryRepository.deleteById(id);
    }
}
