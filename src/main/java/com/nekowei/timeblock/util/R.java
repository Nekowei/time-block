package com.nekowei.timeblock.util;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@Data
public class R<T> implements Serializable {
    private T result;
    private String message;
    private int status;

    public static R<Void> ok() {
        var r = new R<Void>();
        r.setMessage("ok");
        r.setStatus(200);
        return r;
    }

    public static R<Void> fail(int status, String message) {
        return new R<>(null, message, status);
    }

    public R(T t) {
        this.status = 200;
        this.message = "ok";
        this.result = t;
    }

    public R(T t, String message) {
        this.status = 200;
        this.message = message;
        this.result = t;
    }

    public R(T t, String message, int status) {
        this.status = status;
        this.message = message;
        this.result = t;
    }

}
