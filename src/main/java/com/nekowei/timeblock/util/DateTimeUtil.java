package com.nekowei.timeblock.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {

    public static final String format = "yyyy-MM-dd HH:mm:ss";
    public static final String monthFormat = "yyyy-MM";

    public static String now() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(format));
    }

    public static LocalDateTime parse(String datetime) {
        return LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern(format));
    }

    public static String toMonth(LocalDateTime datetime) {
        return datetime.format(DateTimeFormatter.ofPattern(monthFormat));
    }

}
