package com.nekowei.timeblock.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class TypeMapVo {

    public String type;
    public List<StatVo> list;

}
