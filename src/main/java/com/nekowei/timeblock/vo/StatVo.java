package com.nekowei.timeblock.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StatVo {
    private String name;
    private String color;
    private Integer count;
}
