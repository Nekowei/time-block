package com.nekowei.timeblock.repo;

import com.nekowei.timeblock.entity.BlockTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlockTypeRepository extends JpaRepository<BlockTypeEntity, Integer> {
}
