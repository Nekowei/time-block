package com.nekowei.timeblock.repo;

import com.nekowei.timeblock.entity.BlockEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlockRepository extends JpaRepository<BlockEntity, Integer> {

    List<BlockEntity> findByUsernameAndRecordDateBetween(String username, String from, String to, Sort by);

}
