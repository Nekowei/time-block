package com.nekowei.timeblock.web;

import com.nekowei.timeblock.service.BlockService;
import com.nekowei.timeblock.vo.StatVo;
import com.nekowei.timeblock.vo.TypeMapVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("stat")
public class StatController {

    @Autowired
    private BlockService blockService;

    @GetMapping("month")
    public ResponseEntity<List<StatVo>> month(@RequestParam(value = "day", required = false)Integer day) {
        LocalDate date = getDate(day);
        int dom = date.getDayOfMonth();
        int max = date.getMonth().length(date.isLeapYear());
        return new ResponseEntity<>(blockService.stat(date.minusDays(dom), date.plusDays(max - dom)), HttpStatus.OK);
    }

    @GetMapping("recent30")
    public ResponseEntity<List<StatVo>> recent30(
            @RequestParam(value = "day", required = false)Integer day,
            @RequestParam(value = "length", required = false)Integer length) {
        if (length == null) {
            length = 30;
        }
        LocalDate date = getDate(day);
        return new ResponseEntity<>(blockService.statDot(date.minusDays(length), date), HttpStatus.OK);
    }

    @GetMapping("monthMajor")
    public ResponseEntity<List<StatVo>> monthMajor(@RequestParam(value = "day", required = false)Integer day) {
        LocalDate date = getDate(day);
        int dom = date.getDayOfMonth();
        int max = date.getMonth().length(date.isLeapYear());
        return new ResponseEntity<>(blockService.statMajor(date.minusDays(dom), date.plusDays(max - dom)), HttpStatus.OK);
    }

    @GetMapping("major")
    public ResponseEntity<List<StatVo>> major(@RequestParam(value = "from")String from, @RequestParam(value = "to")String to) {
        return new ResponseEntity<>(blockService.statMajor(LocalDate.parse(from), LocalDate.parse(to)), HttpStatus.OK);
    }

    @GetMapping("week")
    public ResponseEntity<List<TypeMapVo>> week(@RequestParam(value = "day", required = false)Integer day) {
        LocalDate date = getDate(day);
        var list1 = blockService.statMajor(date.minusDays(27), date.minusDays(21));
        var list2 = blockService.statMajor(date.minusDays(20), date.minusDays(14));
        var list3 = blockService.statMajor(date.minusDays(13), date.minusDays(7));
        var list4 = blockService.statMajor(date.minusDays(6), date);
        var map = list1.stream()
                .map(vo -> {
                    var list = new ArrayList<StatVo>();
                    list.add(vo);
                    list2.stream().filter(v -> v.getName().equals(vo.getName()))
                            .findAny().ifPresent(list::add);
                    list3.stream().filter(v -> v.getName().equals(vo.getName()))
                            .findAny().ifPresent(list::add);
                    list4.stream().filter(v -> v.getName().equals(vo.getName()))
                            .findAny().ifPresent(list::add);
                    return new TypeMapVo(vo.getName(), list);
                })
                .collect(Collectors.toList());
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    private LocalDate getDate(Integer day) {
        if (day == null) {
            day = 0;
        }
        return LocalDate.now().plusDays(day);
    }

}
