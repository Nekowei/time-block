package com.nekowei.timeblock.web;

import com.nekowei.timeblock.entity.DiaryEntity;
import com.nekowei.timeblock.service.DiaryService;
import com.nekowei.timeblock.util.R;
import com.nekowei.timeblock.util.UserUtil;
import com.nekowei.timeblock.vo.DiaryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("diary")
public class DiaryController {

    @Autowired
    private DiaryService diaryService;

    @PostMapping("save")
    public R<Void> save(@ModelAttribute DiaryEntity e) {
        e.setUsername(UserUtil.getUsername());
        diaryService.save(e);
        return R.ok();
    }

    @GetMapping("list")
    public R<List<DiaryVo>> list() {
        return new R<>(diaryService.list(UserUtil.getUsername()));
    }


}
