package com.nekowei.timeblock;

import com.nekowei.timeblock.entity.DiaryEntity;
import com.nekowei.timeblock.service.DiaryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DiaryTests {

	@Autowired
	private DiaryService diaryService;

	private final String username = "test";

	@Test
	void save() {
		var e = new DiaryEntity();
		e.setContent("rua");
		e.setUsername(username);
		e = diaryService.save(e);
		//diaryService.delete(e.getId());
	}

	@Test
	void list() {
		diaryService.list(username)
				.forEach(System.out::println);
	}

}
