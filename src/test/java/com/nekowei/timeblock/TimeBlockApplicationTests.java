package com.nekowei.timeblock;

import com.nekowei.timeblock.entity.BlockEntity;
import com.nekowei.timeblock.entity.BlockTypeEntity;
import com.nekowei.timeblock.repo.BlockRepository;
import com.nekowei.timeblock.service.BlockService;
import com.nekowei.timeblock.service.BlockTypeService;
import com.nekowei.timeblock.vo.BlockVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@SpringBootTest
class TimeBlockApplicationTests {

	@Autowired
	private BlockService blockService;
	@Autowired
	private BlockTypeService blockTypeService;
	@Autowired
	private BlockRepository blockRepository;

	private final String username = "test";

	@Test
	void saveType() {
		BlockTypeEntity e = new BlockTypeEntity();
		e.setName("rua");
		e.setColor("#ffeedd");
		e.setUsername(username);
		e = blockTypeService.save(e);
		blockTypeService.delete(e.getId());
	}

	@Test
	void saveTypeDetail() {
		BlockTypeEntity e = new BlockTypeEntity();
		e.setName("mua");
		e.setColor("#ffeedd");
		e.setParentId(1);
		e.setUsername(username);
		e = blockTypeService.save(e);
		blockTypeService.delete(e.getId());
	}

	@Test
	void saveBlock() {
		BlockEntity e = new BlockEntity();
		e.setRecordDate(LocalDate.now().toString());
		e.setStartTime(LocalTime.of(21, 0).format(DateTimeFormatter.ISO_LOCAL_TIME));
		e.setEndTime(LocalTime.of(21, 15).format(DateTimeFormatter.ISO_LOCAL_TIME));
		e.setTypeId(1);
		e.setUsername(username);
		e = blockService.save(e);
		blockService.delete(e);
	}

	@Test
	void saveAllBlock() {
		BlockVo e = new BlockVo();
		e.setRecordDate(LocalDate.now());
		e.setHour(23);
		e.setTypeId(1);
		blockService.saveAll(e, username)
				.forEach(be -> blockService.delete(be));
	}

	@Test
	void statBetween() {
		List<BlockEntity> list = blockRepository.findByUsernameAndRecordDateBetween(
				"nekowei",
				LocalDate.now().minusDays(20).toString(),
				LocalDate.now().plusDays(7 - 2).toString(), Sort.by("recordDate", "startTime"));
		System.out.println(list.size());
	}

	@Test
	void list() {
		blockService.list(LocalDate.of(2023, 6, 5), "nekowei")
				.forEach((k, v) -> System.out.println(k + ": " + v));
	}

}
