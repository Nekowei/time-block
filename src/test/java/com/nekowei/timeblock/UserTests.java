package com.nekowei.timeblock;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

@SpringBootTest
public class UserTests {

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    JdbcUserDetailsManager userDetailsManager;

    @Test
    public void update() {
        var user = userDetailsManager.loadUserByUsername("walpurgis");
        userDetailsManager.updateUser(User.builder()
                        .username(user.getUsername())
                        .password(passwordEncoder.encode("123456"))
                        .authorities(user.getAuthorities())
                .build());
    }

}
